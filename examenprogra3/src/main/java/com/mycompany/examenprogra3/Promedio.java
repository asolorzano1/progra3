package com.mycompany.examenprogra3;

import com.mycompany.examenprogra3.examen.Serie1;
import java.util.ArrayList;
import java.util.List;
import rx.Observable;
import rx.observables.MathObservable;

/**
 *
 * @author solor
 */
public class Promedio {

    public static void main(String[] args) {
        
        List<Serie1> num = new ArrayList<>();
        num.add(new Serie1 (1, 300));
        num.add(new Serie1 (2, 300));
        num.add(new Serie1 (3, 200));
        num.add(new Serie1 (4, 800));
        num.add(new Serie1 (5, 230));
        

        Observable<Serie1> serObservable = Observable.from(num);

        MathObservable
                .from(serObservable)
                .averageInteger(Serie1::getPrecio)
                .subscribe((promedio) -> {
                    System.out.println("PROMEDIO:" + promedio);
                });
    }
}
