package com.mycompany.examenprogra3;

import rx.Observable;
import rx.functions.Action1;
import rx.functions.Func1;

/**
 *
 * @author solor
 */
public class ProductosConA {

    public static void main(String[] args) {

        String[] nombre = {"vehiculo simple", "vehiculo simple aut", "vehiculo doble tracccion", "vehiculo alta gama", "motocicleta"};

        Observable.from(nombre).filter(

        new Func1<String, Boolean>() {
            @Override
            public Boolean call(String t) {

                return t.contains("a");
            }

                }// ordenamiento
        ).sorted().subscribe(
        new Action1<String>() {

            @Override
            public void call(String s) {

                System.out.println("FILTRO " + s + "!");
            }

        });
    }  
}
