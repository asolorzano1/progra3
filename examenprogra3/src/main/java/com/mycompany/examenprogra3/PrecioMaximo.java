package com.mycompany.examenprogra3;

import rx.Observable;
import rx.functions.Action1;
import rx.functions.Func1;
/**
 *
 * @author solor
 */
public class PrecioMaximo {

    public static void main(String[] args) {
        int[] num = {300, 300, 200, 800, 230};
        Observable.from(num).filter(

        new Func1<int, Boolean>() {
            @Override
            public Boolean call(String t) {

                return t.contains("a");
            }

                }// ordenamiento
        ).sorted().subscribe(
        new Action1<String>() {

            @Override
            public void call(String s) {

                System.out.println("FILTRO " + s + "!");
            }

        });
    }
    
}
