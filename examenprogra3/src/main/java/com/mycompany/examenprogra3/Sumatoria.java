package com.mycompany.examenprogra3;

import rx.Observable;
import rx.functions.Func2;

/**
 *
 * @author solor
 */
public class Sumatoria {

    public static void main(String[] args) {
       Integer[] numbers = {300, 300, 200, 800, 230};

        Integer resultado;

        Observable miobservable = Observable.from(numbers).reduce(
                new Func2<Integer, Integer, Integer>() {
                    @Override
                    public Integer call(Integer sum, Integer actual) {

                        return sum + actual;
                    }
                }
        );
        miobservable.subscribe((sumatoria) -> {
            System.out.println("RESULTADO:" + sumatoria);
        });
    }
}
