package com.mycompany.examenprogra3.examen;

/**
 *
 * @author solor
 */
public class Serie1 {
    private int id;
    private int precio;

    public Serie1() {
    }

    public Serie1(int id, int precio) {
        this.id = id;
        this.precio = precio;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getPrecio() {
        return precio;
    }

    public void setPrecio(int precio) {
        this.precio = precio;
    }

}
